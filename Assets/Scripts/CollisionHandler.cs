using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{

    AudioSource rocketAudio;

    [SerializeField] float levelDelay = 2f;
    [SerializeField] AudioClip crash;
    [SerializeField] AudioClip win;

    void Start()
    {
        rocketAudio = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision other)
    {
        switch (other.gameObject.tag)
        {
            case "Friendly":
                Debug.Log("Touching Friendly");
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            default:
                StartCrashSequence();
                break;
        }
    }

    void StartSuccessSequence()
    {
        GetComponent<Movement>().enabled = false;
        rocketAudio.PlayOneShot(win);
        Invoke("LoadNextLevel", levelDelay);
    }

    void ReloadLevel()
    {
        int curBuildIndex = SceneManager.GetActiveScene().buildIndex;
        
        SceneManager.LoadScene(curBuildIndex);
    }

    void StartCrashSequence()
    {
        //todo sfx and particle
        
        GetComponent<Movement>().enabled = false;
        rocketAudio.PlayOneShot(crash);
        Invoke("ReloadLevel", levelDelay);
    }
    
    void LoadNextLevel()
    {
        int curBuildIndex = SceneManager.GetActiveScene().buildIndex;
        int nextBuildIndex = curBuildIndex + 1;
        if (nextBuildIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextBuildIndex = 0;
        }
        SceneManager.LoadScene(nextBuildIndex);
    }
}