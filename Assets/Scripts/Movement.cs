﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    Rigidbody rocketRigidBody;
    AudioSource rocketAudio;
    [SerializeField] AudioClip mainEngine;


    [SerializeField] float thrust = 1000;
    [SerializeField] float rotSpeed = 0.5f;


    // Start is called before the first frame update
    void Start()
    {
        rocketRigidBody = GetComponent<Rigidbody>();
        rocketAudio = GetComponent<AudioSource>();

        

    }

    // Update is called once per frame
    void Update()
    {
        ProccessThrust();
        ProcessRotate();

    }

    private void ProcessRotate()
    {
        if (Input.GetKey( KeyCode.A))
        {
            ApplyRotation(rotSpeed);

        }
        else if (Input.GetKey(KeyCode.D))
        {
            ApplyRotation(-rotSpeed);

        }
    }

    private void ApplyRotation(float rotThisFrame)
    {
        rocketRigidBody.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotThisFrame * Time.deltaTime);
        rocketRigidBody.freezeRotation = false;
    }

    private void ProccessThrust()
    {

        if (Input.GetKey(KeyCode.Space))
        {

            rocketRigidBody.AddRelativeForce(Vector3.up * thrust * Time.deltaTime);

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rocketAudio.PlayOneShot(mainEngine);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            rocketAudio.Stop();
        }

    }
}